# IP on IOTA

Publish the local IP address received on startup to the IOTA Tangle.
The script will auto-generate a seed, and will post all messages to the first address from that seed.

## Quickstart

The quickest way to deploy the script to your device is through the [run.sh](run.sh).

### Quickstart Prerequisites

* Docker >= 19

### Quickstart Steps

1. Update the `ansible_host` and `ansible_user` variables in [inventories/rpi.yml](inventories/rpi.yml) to your device IP/domain and user respectfully.
2. Execute the following to deploy IP on IOTA using the default tag `IPONIOTA`.

    ```bash
    ./run.sh
    ```

    Alternatively, you can deploy IP on IOTA using a custom tag by adding the custom tag as an argument.

    ```bash
    ./run.sh MYCUSTOMTAG
    ```

    *NOTE:* Your custom tag must only contain uppercase letters and cannot be longer than 27 letters.
3. Refer to the [Searching the Tangle](#searching-the-tangle) section for more information on how to find your IP by the Tag.

## Searching the Tangle

1. Open the Tangle Explorer: Main Net - <https://thetangle.org>
2. Search for your tag - if you did not specify a tag, the default `IPONIOTA` would have been used
3. Find the correct transaction. You can convert the message into either a string or JSON to view the message
4. Once you find the correct transaction, click on the address value
5. All subsequent messages should be at this address. You can bookmark the link if you so wish

## Advanced Setup

IP on IOTA at it's core is a simple NodeJS project.
The NodeJS project can be found in this repository at [roles/node/files/ip-on-iota](roles/node/files/ip-on-iota).
Ansible is used to configure the device and deploy IP on IOTA.

### Dockerless Deployment

The [Quickstart](#quickstart) makes use of Docker to wrap the Ansible execution.
If you don't want to use Docker you can invoke Ansible directly.

#### Dockerless Prerequisites

* Python >= 3.7
* Pip >= 18.1
* Ansible >= 2.8
* sshpass >= 1.06

#### Dockerless Steps

1. Update the `ansible_host` and `ansible_user` variables in [inventories/rpi.yml](inventories/rpi.yml) to your device IP/domain and user respectfully.
2. Execute the following to deploy IP on IOTA using the default tag `IPONIOTA`.

    ```bash
    ansible-playbook -i inventories/rpi.yml --ask-pass ip_on_iota.yml
    ```

    Alternatively, you can deploy IP on IOTA using a custom tag by overwriting the default tag variable using `--extra-vars`.

    ```bash
    ansible-playbook -i inventories/rpi.yml --ask-pass ip_on_iota.yml --extra-vars "IpOnIotaTag=MYCUSTOMTAG"
    ```

    *NOTE:* Your custom tag must only contain uppercase letters and cannot be longer than 27 letters.
3. Refer to the [Searching the Tangle](#searching-the-tangle) section for more information on how to find your IP by the Tag.

### Standalone IP on IOTA

If you don't want the overhead of Ansible to orchestrate a deployment, you can manually execute IP on IOTA to publish your IP.
As IP on IOTA is NodeJS project, you can do this by simply using `npm` in the project direcotry.

#### Standalone Prerequisites

* NodeJS >= 8

Refer to [snippet](https://gitlab.com/rpi-utilities/ip-on-iota/snippets/1889518) for help installing NodeJS.

#### Standalone Steps

1. Optional - Set a custom tag to pubilsh you IP to the tangle with. If this is not set the default tag `IPONIOTA` will be used.

    ```bash
    export IP_ON_IOTA_TAG="MYCUSTOMTAG"
    ```

    *NOTE:* Your custom tag must only contain uppercase letters and cannot be longer than 27 letters.
2. Execute the following to install package dependencies and run the project.

    ```bash
    npm install --only=prod --prefix ./roles/node/files/ip-on-iota
    npm start --prefix ./roles/node/files/ip-on-iota
    ```
