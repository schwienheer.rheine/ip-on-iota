#!/bin/bash -e
# This scripts wraps the docker build and run required to deploy the IP on IOTA utility

logger() {
    # Get arguments
    local msg=$1
    local msgType=$2  # Optional message type

    # ANSI colour escape codes
    local yellow='\033[1;33m'
    local red='\033[1;31m'
    local cyan='\033[1;36m'
    local normal='\033[0m'
    local green='\033[1;32m'

    # Determine message type
    case $msgType in
        warning)
            msgPrefix="${yellow}[WARNING]${normal}"
            ;;
        error)
            msgPrefix="${red}[ERROR]${normal}"
            ;;
        success)
            msgPrefix="${green}[SUCCESS]${normal}"
            ;;
        *)
            msgPrefix="${cyan}[INFO]${normal}"
            ;;
    esac

    # Output log message
    echo -e "$msgPrefix $msg"
}

usage() {
    logger "Unexpected number of arguments" "error"
    echo "Usage: $(basename "$0") [TAG]"
    exit 1
}

validateTag() {
    if [[ ! $1 =~ ^[A-Z9]{1,27}$ ]]
    then
        logger "TAG argument must only contain uppercase letters and cannot be longer than 27 letters" "error"
        exit 1
    fi
}

# Check usage and get arguments
expectedArgs=1
if [[ $# -gt $expectedArgs ]]
then
    usage
elif [[ $# -eq $expectedArgs ]]
then
    validateTag $1
    additionalArgs="-e IpOnIotaTag=$1"
    logger "Deploying with custom tag"
fi

# Build and deploy
logger "Building container"
docker build -t ip-on-iota-deployer:latest .

logger "Deploying IP on IOTA"
docker run -it ip-on-iota-deployer:latest $additionalArgs

logger "IP on IOTA deployed" "success"
